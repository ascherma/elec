from django.db import models

from politicians import models as politicians
from issues import models as issues

# Create your models here.

class News(models.Model):
	headline = models.CharField('Manchete', max_length=255)
	link = models.URLField('Link')
	# Related to politicians models
	related_politicians = models.ManyToManyField(politicians.Candidate, verbose_name='Políticos relacionados')
	related_parties = models.ManyToManyField(politicians.Party, verbose_name='Partidos relacionados')
	# Related to issues models
	related_issues = models.ManyToManyField(issues.Issue, verbose_name='Questões relacionadas')
	related_positionings = models.ManyToManyField(issues.Positioning, verbose_name='Posições relacionadas')
	
	date = models.DateField('Data')

	class Meta:
		verbose_name = 'Notícias'
		verbose_name_plural = 'Notícias'
