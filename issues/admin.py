from django.contrib import admin

from .models import Issue, Positioning
# Register your models here.

@admin.register(Issue)
class IssueAdmin(admin.ModelAdmin):
	list_display = ('title',)
	fieldsets = (
		(None, {
			'fields': (
				'title',
			)
		}),
	)


@admin.register(Positioning)
class PositioningAdmin(admin.ModelAdmin):
	list_display = ('issue', 'candidate')
	search_fields = ('issue', 'candidate')
	fieldsets = (
		(None, {
			'fields': (
				'candidate',
				'issue',
				'positioning',
			)
		}),
	)