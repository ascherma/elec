from django.db import models

# Create your models here.

class Party(models.Model):
	name = models.CharField('Nome', max_length=100)
	acronymins = models.CharField('Sigla', max_length=10)

	def __str__(self):
		return self.acronymins

	class Meta:
		verbose_name = 'Partido'
		verbose_name_plural = 'Partidos'


class Candidate(models.Model):
	PRESIDENT = 'a'
	SENATOR = 'b'
	DEPUTY = 'c'
	POSITIONS = (
		(PRESIDENT, 'Presidente'),
		(SENATOR, 'Senador'),
		(DEPUTY, 'Deputado federal'),
	)

	name = models.CharField('Nome', max_length=100)
	competes_for = models.CharField('Concorre ao cargo de', max_length=1, choices=POSITIONS)
	party = models.ForeignKey(Party, related_name='member_of', verbose_name='Partido')
	prev_parties = models.ManyToManyField(Party, related_name='former_member_of', verbose_name='Partidos anteriores')

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Candidato'
		verbose_name_plural = 'Candidatos'


class Term(models.Model):
	PRESIDENT = 'a'
	SENATOR = 'b'
	FED_DEPUTY = 'c',
	STD_DEPUTY = 'd',
	GOVERNOR = 'e',
	MAYOR = 'f',
	COUNCILOR = 'g'

	POSITIONS = (
		(PRESIDENT, 'Presidente'),
		(SENATOR, 'Senador'),
		(FED_DEPUTY, 'Deputado federal'),
		(STD_DEPUTY, 'Deputado estadual'),
		(GOVERNOR, 'Governador'),
		(MAYOR, 'Prefeito'),
		(COUNCILOR, 'Vereador'),
	)

	END_OF_TERM_TIME = 'a'
	DEATH = 'b'
	HEALTH_ISSUES = 'c'
	RESIGNATION = 'd'
	CASSATION = 'e'

	END_OF_TERM_MOTIVES = (
		(END_OF_TERM_TIME, 'Fim do tempo de mandato'),
		(DEATH, 'Morte'),
		(HEALTH_ISSUES, 'Problemas de saúde'),
		(RESIGNATION, 'renúnica'),
		(CASSATION, 'Cassação'),
	)

	politician = models.ForeignKey(Candidate)
	position = models.CharField('Cargo', max_length=1, choices=POSITIONS)
	party_at_time = models.ForeignKey(Party, verbose_name='Partido à época')
	start_year = models.PositiveSmallIntegerField('Ano de início')
	end_year = models.PositiveSmallIntegerField('Ano de término', blank=True, null=True)
	end_of_term_motive = models.CharField('Motivo do término', max_length=1, choices=END_OF_TERM_MOTIVES, blank=True, null=True)

	class Meta:
		verbose_name = 'Mandato'
		verbose_name_plural = 'Mandatos'

		unique_together = ('politician', 'start_year')

