from django.contrib import admin


from .models import News
# Register your models here.

@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
	list_display = ('headline', 'date')
	fieldsets = (
		(None, {
			'fields': (
				'headline',
				'link',
				'related_politicians',
				'related_parties',
				'related_issues',
				'related_positionings',
				'date',
			)
		}),
	)