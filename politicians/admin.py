from django.contrib import admin

from .models import Party, Candidate, Term
# Register your models here.

@admin.register(Party)
class PartyAdmin(admin.ModelAdmin):
	list_display = ('name', 'acronymins')
	fieldsets = (
		(None, {
			'fields': (
				'name',
				'acronymins',
			)
		}),
	)


@admin.register(Candidate)
class CandidateAdmin(admin.ModelAdmin):
	list_display = ('name', 'competes_for', 'party')
	search_fields = ('competes_for', 'party')
	fieldsets = (
		(None, {
			'fields': (
				'name',
				'competes_for',
				'party',
			)
		}),
	)

@admin.register(Term)
class TermAdmin(admin.ModelAdmin):
	list_display = ('position', 'start_year', 'politician')
	fieldsets = (
		(None, {
			'fields': (
				'politician',
				'position',
				'party_at_time',
				'start_year',
				'end_year',
				'end_of_term_motive',
			)
		}),
	)
