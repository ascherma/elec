from django.db import models

from politicians.models import Candidate

# Create your models here.

class Issue(models.Model):
	title = models.CharField('Título', max_length=100)

	def __str__(self):
		return self.title

	class Meta:
		verbose_name = 'Questão'
		verbose_name_plural = 'Questões'


class Positioning(models.Model):

	FAVORABLE = 'a'
	OPPOSED = 'b'
	ABSTAIN = 'c'

	POSITIONINGS = (
		(FAVORABLE, 'Favorável'),
		(OPPOSED, 'Contrário'),
		(ABSTAIN, 'Abstenção')
	)

	candidate = models.ForeignKey(Candidate)
	issue = models.ForeignKey(Issue)
	positioning = models.CharField('Posicionamento', max_length=1, choices=POSITIONINGS)

	def __str__(self):
		return 'Posição de ' + self.candidate.name + ' sobre ' + self.issue.title

	class Meta:
		verbose_name = 'Posicionamento'
		verbose_name_plural = 'Posicionamentos'
		unique_together = ('candidate', 'issue')